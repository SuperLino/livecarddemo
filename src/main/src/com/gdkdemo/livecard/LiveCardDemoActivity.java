package com.gdkdemo.livecard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

import com.gdkdemo.livecard.service.LiveCardDemoService;
import com.gdkdemo.livecard.R;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

// The "main" activity...
public class LiveCardDemoActivity extends Activity {
	// Service to handle liveCard publishing, etc...
	// private LiveCardDemoService liveCardDemoService;

	private static final int STOPPED = 0x0001; // 服务停止的标志数

	// private GestureDetector mGestureDetector; // Glass手势检测

	private MyApplication mApplication = null; // 全局变量，用于Handler共享

	private MyHandler mHandler = null; // Handler处理MenuActivity传输的信息

	private TextView mTextView;

	// 开始服务
	public void doStartService() {
		startService(new Intent(this, LiveCardDemoService.class));
	}

	// 停止服务
	public void doStopService() {
		stopService(new Intent(this, LiveCardDemoService.class));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_livecarddemo);

		mApplication = (MyApplication) getApplication();
		mHandler = new MyHandler();
		mApplication.setHandler(mHandler);

		doStartService();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// mGestureDetector = createGestureDetector(this);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			startMenu();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	// private GestureDetector createGestureDetector(Context context) {
	// GestureDetector gestureDetector = new GestureDetector(context);
	// // Create a base listener for generic gestures
	// gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
	// @Override
	// public boolean onGesture(Gesture gesture) {
	// if (gesture == Gesture.TAP) {
	// startMenu();
	// return true;
	// }
	// return false;
	// }
	//
	// });
	//
	// return gestureDetector;
	// }

	/*
	 * Send generic motion events to the gesture detector //
	 */
	// @Override
	// public boolean onGenericMotionEvent(MotionEvent event) {
	// if (mGestureDetector != null) {
	// return mGestureDetector.onMotionEvent(event);
	// }
	// return false;
	// }

	public void startMenu() {
		Intent mIntent = new Intent(this, MenuActivity.class);
		startActivity(mIntent);
	}

	final class MyHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == STOPPED) { // ??????UI
				mTextView = (TextView) findViewById(R.id.screen_content);
				mTextView.setText("LiveCard 已停止");

			}
		}

	}
}
