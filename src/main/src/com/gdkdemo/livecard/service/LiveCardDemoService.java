package com.gdkdemo.livecard.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.timeline.TimelineManager;
import com.gdkdemo.livecard.LiveCardDemoActivity;
import com.gdkdemo.livecard.R;

// ...
public class LiveCardDemoService extends Service {
	// "Life cycle" constants

	// [1] Starts from this..
	//private static final int STATE_NORMAL = 1;

	// Global "state" of the service.
	// Currently not being used...
	//private int currentState;

	private boolean run = true;

	private Handler handler = new Handler();

	private TimelineManager mTimelineManager;
	// For live card
	private LiveCard liveCard;

	private RemoteViews mRemoteViews;

	public LiveCardDemoService() {
		super();
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		onServiceStart();
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		onServiceStart();
		return null;
	}

	@Override
	public void onDestroy() {
		onServiceStop();

		super.onDestroy();
	}

	// Service state handlers.
	// ....

	private boolean onServiceStart() {

		// 发布live card
		publishCard(this);

		return true;
	}

	private boolean onServiceStop() {
		run = false;
		// 回收 livecard
		unpublishCard(this);

		return true;
	}

	// For live cards...

	private void publishCard(Context context) {
		if (liveCard == null) {
			String cardId = "livecarddemo_card";
			mTimelineManager = TimelineManager.from(context);
			liveCard = mTimelineManager.createLiveCard(cardId);

			mRemoteViews = new RemoteViews(context.getPackageName(),
					R.layout.activity_livecarddemo);
			liveCard.setViews(mRemoteViews);

			Intent intent = new Intent(context, LiveCardDemoActivity.class);
			liveCard.setAction(PendingIntent.getActivity(context, 0, intent, 0));
			
			liveCard.publish(LiveCard.PublishMode.REVEAL);

			handler.post(myRunnable);

		} else {
			// Card is already published.
			return;
		}
	}

	int count = 0;

	private Runnable myRunnable = new Runnable() {
		public void run() {

			if (run) {
				handler.postDelayed(this, 1000);
				count++;

				mRemoteViews.setTextViewText(R.id.screen_content, "喜大普奔"
						+ count);
				liveCard.setViews(mRemoteViews);

				Log.e("LiveCardDemoService", String.valueOf(count).toString());
			}
		}
	};

	private void unpublishCard(Context context) {
		if (liveCard != null) {
			liveCard.unpublish();
			liveCard = null;
		}
	}

}
