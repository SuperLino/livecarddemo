package com.gdkdemo.livecard;  
  

import com.gdkdemo.livecard.LiveCardDemoActivity.MyHandler;

import android.app.Application;  
   
public class MyApplication extends Application {  

	private MyHandler handler = null;  
      
    public void setHandler(MyHandler handler) {  
        this.handler = handler;  
    }  
      
    public MyHandler getHandler() {  
        return handler;  
    }  
}  