package com.gdkdemo.livecard;

import com.gdkdemo.livecard.R;
import com.gdkdemo.livecard.LiveCardDemoActivity.MyHandler;
import com.gdkdemo.livecard.service.LiveCardDemoService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MenuActivity extends Activity {

	private static final int STOPPED = 0x0001;

	private MyApplication myApplication = null;

	private MyHandler mHandler = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		myApplication = (MyApplication) getApplication();
		// ???????????????????????????
		mHandler = myApplication.getHandler();
	
	}

	@Override
	protected void onResume() {
		super.onResume();

		openOptionsMenu();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	// ??????
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_livecarddemo_livecard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// ??????????????????
		switch (item.getItemId()) {
		case R.id.menu_stop_livecarddemo:
			mHandler.sendEmptyMessage(STOPPED);

			stopService(new Intent(this, LiveCardDemoService.class));

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		// Nothing else to do, closing the activity.
		finish();
	}
}
